const problem4 = (users) => {
  if (typeof users == "object") {
    let languages = {
      Javascript: [],
      Python: [],
      Golang: [],
    };
    for (let user in users) {
      let designation = users[user]["desgination"];
      if (designation.includes("Python")) {
        languages["Python"].push(user);
      } else if (designation.includes("Golang")) {
        languages["Golang"].push(user);
      } else if (designation.includes("Javascript")) {
        languages["Javascript"].push(user);
      }
    }
    return languages;
  } else {
    return;
  }
};

export default problem4;
