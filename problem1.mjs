let requiredInterest = "Playing Video Games";
let n = requiredInterest.length;

const checkInterest = (interest) => {
  for (let i = 0; i < interest.length; i++) {
    let j = 0;
    while (interest[i + j] == requiredInterest[j] && j < n) {
      j++;
      if (j == n) {
        return true;
      }
    }
  }
};

const problem1 = (users) => {
  if (typeof users == "object") {
    const interestedUsers = [];
    for (let user in users) {
      if (Array.isArray(users[user]["interests"])) {
        for (let interest of users[user]["interests"]) {
          if (checkInterest(interest)) {
            //checkInterest function will check whether the interest is
            //playing video games or not and return a boolean value
            interestedUsers.push(user);
          }
        }
      }
    }
    return interestedUsers;
  } else {
    return;
  }
};

export default problem1;
