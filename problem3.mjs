const problem3 =(users)=>{
    if(typeof users=='object'){
        let masterDegreeUsers=[]
        for(let user in users){
            if(users[user]['qualification']=='Masters'){
                masterDegreeUsers.push(user)
            }
        }
        return masterDegreeUsers
    }else{
        return null;
    }
}

export default problem3