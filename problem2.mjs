const problem2 = (users) => {
  if (typeof users == "object") {
    let germanyUser = [];
    for (let user in users) {
      if (users[user]["nationality"] == "Germany") {
        germanyUser.push(user);
      }
    }
    return germanyUser;
  } else {
    return;
  }
};

export default problem2;
